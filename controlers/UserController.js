const io = require('../io');

const crypt = require('../crypt');

const requestJson = require('request-json');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edjada/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getUsersV1 (req, res) {
  console.log("GET /apitechu/v1/users");

  var result = {};
  var users = require('../usuarios.json');

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
    users.slice(0, req.query.$top) : users;

    res.send(result);
  }

function createUserV1(req, res) {
    console.log("POST /apitechu/v1/users");

    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);
    console.log(req.body.password);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password
    };

    var users = require('../usuarios.json');
    users.push(newUser);
    io.writeUserDataToFile(users);
    res.send("usuarios añadido con exito");
  }

  function deleteUsersV1 (req, res) {

    console.log("DELETE /apitechu/v1/users/:id");

    console.log("El id a borrarr es: " + req.params.id);

    var users  = require("../usuarios.json");

    //splice quita los elementos del arrray que le indiques en la posicion que le indiques
    //users.splice(req.params.id -1, 1);
    //con bucle for sin mas
    /*console.log(users.length);
    for (var i = 0; i < users.length; i++)
    {
      console.log(users[i].id);
      console.log(req.params.id);
      if (users[i].id == req.params.id)
      {
        console.log("he borrado");
        users.splice(i, 1);
        break;
      }
    }*/
    //con blucke For In recorre los elementos del objeto sin un orden en concreto
    /*for (const obj in users)
    {
      console.log(users[obj].id);
      console.log(req.params.id);
      if (users[obj].id == req.params.id)
      {
        console.log("he borrado");
        users.splice(obj, 1);
        break;
      }
    }*/
    // bucle for of
    /*var aux = 0;
    for (const value of users)
    {
      console.log(value.id);
      console.log(req.params.id);
      console.log(aux);
      if (value.id == req.params.id)
      {
        console.log("he borrado");
        console.log(aux);
        users.splice(aux, 1);
        break;
      }
      aux++;
    }*/
    // bucle array for each
    /*function borrararray (element , index , array)
    {
      console.log(element.id);
      console.log(req.params.id);
      if (element.id == req.params.id)
      {
        console.log("he borrado");
        array.splice(index, 1);
      }
    }
    users.forEach(borrararray);*/

    //bucle array find index
    var deleted = false;

    function findelement (element , index , array)
    {
      console.log(element.id);
      console.log(req.params.id);
      if (element.id == req.params.id)
      {
        return true;
      }
    }
    var index = users.findIndex(findelement);
    console.log(index);

    if (index >= 0)
    {
      users.splice(index, 1);
      console.log("Usuario Borrado");
      deleted = true;
    }
    else
    {
      console.log("Usuario NO Encontrado");
      deleted = false;
    }

    io.writeUserDataToFile(users);

    var msg = deleted ? "Usuario borrado" : "Usuario no encontrado";
    res.send({"msg" : msg});
  }

  function getUsersV2 (req, res) {
    console.log("GET /apitechu/v2/users");

    var httpClient = requestJson.createClient(mlabBaseURL);
    console.log("Client Created");

    httpClient.get("user?" + mlabAPIKey,
      function(err,resMlab,body){
        var response = !err ?
        //aqui prodirmamos gestionar los codigos de error diferentes dentro del objeto resMlab que contiene toda la informacion de la respuesta no solo el body
          body : {"msg" : "Error obtenido de usuarios"};
          res.send(response);
      }
    )

}

  function getUserByIdV2 (req, res) {
    console.log("GET /apitechu/v2/getUserByIdV2");

    var httpClient = requestJson.createClient(mlabBaseURL);
    console.log("Client Created");
    var id = req.params.id;
    console.log(id);
    var query = 'q={"id": '+ id +'}';
    console.log("user?" + query + "&" + mlabAPIKey);


    httpClient.get("user?" + query + "&" + mlabAPIKey,
      function(err,resMlab,body){

        if(err){
          var response = {
              "msg" : "Error obteniendo Usuario"
          };
          res.status(500);
        } else {
          if (body.length > 0){
            var response = body[0];
          }else {
            var response = {
              "msg" : "Usuario no encontrado"
            };
            res.status(404);
          }
        }
        res.send(response);
      }
    )

}

function createUserV2(req, res) {
    console.log("POST /apitechu/v2/users");
    console.log(req.body.id);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);
    console.log(req.body.password);

    var newUser = {
      "id" :req.body.id,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : crypt.hash(req.body.password)
    };

    var httpClient = requestJson.createClient(mlabBaseURL);
    console.log("Client Created");

    httpClient.post("user?" + mlabAPIKey, newUser,
      function(err,resMlab,body){
        console.log("usuario guardado con exito");
        res.status(201);
        res.send({"msg" : "Usuario creado con exito"});
      }
    )

  }

  module.exports.getUsersV1 = getUsersV1;
  module.exports.createUserV1 = createUserV1;

  module.exports.deleteUsersV1  = deleteUsersV1;
  module.exports.getUsersV2 = getUsersV2;
  module.exports.getUserByIdV2 = getUserByIdV2;
  module.exports.createUserV2 = createUserV2;
