const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
//esto lanza la api para poder ahcer los test automaticos sin necesidad de lanzarlo por fuera.
var server = require('../server.js');

describe('First test',
  function(){
    it('Test that google works', function(done){
        chai.request('https://www.google.com')
          .get('/')
          .end(
            function (err,res){
              console.log("Request Finished");
              // console.log(res);
              console.log(err);
              res.should.have.status(200);
              done();
            }
          )
      }
    )
  }
)

describe('Test de API de Usuarios',
  function(){
    it('Prueba que la API de usuarios responde', function(done){
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/hello')
          .end(
            function (err,res){
              console.log("Request Finished");
              // console.log(res);
              console.log(err);
              // el shoul have es una asercion
              res.should.have.status(200);
              res.body.msg.should.be.eql("hello world");
              done();
            }
          )
      }
    ),
    it('Prueba de que la API devuelve una lista de usuarios correcta', function(done){
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/users')
          .end(
            function (err,res){
              console.log("Request Finished");
              // console.log(res);
              console.log(err);
              res.should.have.status(200);
              for (user of res.body.users){
                user.should.have.property("first_name");
                user.should.have.property("last_name");
                user.should.have.property("email");
                user.should.have.property("password");
              }
              done();
            }
          )
      }
    )
  }
)
